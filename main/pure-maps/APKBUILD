# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=pure-maps
pkgver=2.0.0
pkgrel=0
_commit_geomag="8eb9a730c8643fb7d63fdee4fd9a195ee8ba4df2"
pkgdesc="Maps and navigation"
url="https://github.com/rinigus/pure-maps"
# armhf blocked by mapbox-gl-qml -> qt5-qtdeclarative-dev
arch="all !armhf"
license="GPL-3.0-or-later"
depends="
	kirigami2
	mapbox-gl-qml
	nemo-qml-plugin-dbus
	py3-gpxpy
	py3-pyotherside
	qml-module-clipboard
	qmlrunner
	qt5-qtbase-sqlite
	qt5-qtlocation
	qt5-qtmultimedia
	qt5-qtsensors
	"
makedepends="
	gettext
	py3-pyflakes
	python3
	qt5-qtbase-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qttools-dev
	qtchooser
	"
subpackages="$pkgname-lang"
source="https://github.com/rinigus/pure-maps/archive/$pkgver/pure-maps-$pkgver.tar.gz
	https://github.com/rinigus/geomag/archive/$_commit_geomag/geomag-$_commit_geomag.tar.gz
	"
options="!check" # Requires jsonlint which is not available

prepare() {
	default_prepare

	rmdir thirdparty/geomag
	mv "$srcdir/geomag-$_commit_geomag" thirdparty/geomag
}

build() {
	qmake DEFAULT_BASEMAP=OpenCycleMap DEFAULT_ROUTER=OSRM FLAVOR=kirigami PREFIX=/usr
	make
}

check() {
	make test
}

package() {
	INSTALL_ROOT="$pkgdir" make install

	# Locales get installed to the wrong location and thus have to be moved
	# to get picked up by abuild lang()
	mv "$pkgdir"/usr/share/pure-maps/locale "$pkgdir"/usr/share
}

sha512sums="5018e8cc07d78092002592e616931feb4228ec3cee8a95abac5c5f7f0b03dfb6707964a7d287eb89c2aebaedc7b9a5c55ec5a5d62ecd1c6a3a65c8667c1ac580  pure-maps-2.0.0.tar.gz
13e11b6cb35162315deb86c6c6240a3555760397d7aa88ac9c3348d476e9e9547b03210134119c60790511489e3f2a13afb93a3c77d40b1258c664b6fcc0425c  geomag-8eb9a730c8643fb7d63fdee4fd9a195ee8ba4df2.tar.gz"
